﻿using Bytescout.PDFExtractor;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertPdfExcel
{
    class Program
    {
        static void Main(string[] args)
        {
            // Create Bytescout.PDFExtractor.XLSExtractor instance
            XLSExtractor extractor = new XLSExtractor();
            extractor.RegistrationName = "demo";
            extractor.RegistrationKey = "demo";

            File.Delete("output.xls");

            // Load sample PDF document
            extractor.LoadDocumentFromFile("conta.pdf");

            // Save the spreadsheet to file
            extractor.SaveToXLSFile("output.xls");

            // Open the spreadsheet in default associated application
            Process.Start("output.xls");
        }
    }
}
